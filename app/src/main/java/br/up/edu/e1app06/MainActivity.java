package br.up.edu.e1app06;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcular(View v){

        EditText cxNota1 = (EditText) findViewById(R.id.txtNota1);
        EditText cxNota2 = (EditText) findViewById(R.id.txtNota2);
        EditText cxNota3 = (EditText) findViewById(R.id.txtNota3);
        EditText cxMedia = (EditText) findViewById(R.id.txtMeida);

        String txtNota1 = cxNota1.getText().toString();
        String txtNota2 = cxNota2.getText().toString();
        String txtNota3 = cxNota3.getText().toString();

        double nota1 = Double.parseDouble(txtNota1);
        double nota2 = Double.parseDouble(txtNota2);
        double nota3 = Double.parseDouble(txtNota3);

        double media = (nota1 + nota2 + nota3) / 3;

        cxMedia.setText(String.valueOf(media));

    }

}





